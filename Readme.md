Package manager for unity combined with strangefx auto-wiring functionality.

Package manager imports packages which implement an arbitrary number of known interfaces.  Strange scans codebase (interfaces and implementation namespaces by default) to see if any strange [Implements(ImyInterface)] attributes are present, and when client code requests them, it will get those values.

Player/editor settings are set per configuration as well, as you need different settings for different vr libraries or other packages (such as no multithreaded rendering for dive sdk or other non-vr package, but you want it on for other configurations).  This has the added benefit of using the same source to build apps with different names, bundles, icons, etc.

Note: 
There is also support for updating the interface mapping through server calls in order to add additional loggers, switch logging settings, or to do more radical changes such as change which audio or other library is used.

Dependencies, and their Licenses, links to source
Their licesnes are listed next to the DLLs in the repo
JsonFX
MIT License
https://bitbucket.org/TowerOfBricks/jsonfx-for-unity3d-git

Strange IOC
Apache2
https://github.com/strangeioc/strangeioc/tree/tip

```
Copyright 2015 Alex Sink

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
