{  
   "packagesToAdd":[  
   ],
   "app_output_name":"interfaces_PC",
   "buildOptions":"CompressTextures",
   "buildConfig":{  
      "bundleIdentifier":"com.asink.interfaces",
      "bundleVersion":"1.0",
      "companyName":"DefaultCompany",
      "defaultInterfaceOrientation":"LandscapeLeft",
      "defaultIsFullScreen":true,
      "defaultScreenHeight":1080,
      "defaultScreenWidth":1920,
      "displayResolutionDialog":"HiddenByDefault",
      "productName":"interfaces",
      "renderingPath":"Forward",
      "scriptingDefineSymbols":[  
         ""
      ],
      "stripUnusedMeshComponents":true,
      "supportedAspectRatios":[  
         "Aspect16by10",
         "Aspect16by9"
      ],
      "use32BitDisplayBuffer":false,
      "useDirect3D11":true,
      "usePlayerLog":true,
	  "graphicsAPIs":["Direct3D11","Direct3D9"]
   },
   "editorUserBuildSettings":{  
      "allowDebugging":false,
      "currentBuildTarget":"StandaloneWindows",
      "currentBuildTargetGroup":"Standalone",
      "development":false,
      "explicitNullChecks":false,
      "subtarget":"ETC2",
      "symlinkLibraries":false
   },
   "InterfaceMapping":{  
      "interfaceMap":{
      },
      "interfaceToMultipleMaps":{  

      },
      "namedStringTypes":{ 
	  "PLATFORM":"Windows", "ANALYTICS_INITIALIZATION":"{\"PRIMARY\":[\"foo\"],\"SECONDARY\":[\"{\\\"androidTrackingCode\\\":\\\"UA-57906201-1\\\",\\\"IOSTrackingCode\\\":\\\"UA-57906201-2\\\",\\\"otherTrackingCode\\\":\\\"UA-57906201-3\\\",\\\"productName\\\":null,\\\"bundleIdentifier\\\":\\\"com.asink.interfaces\\\",\\\"bundleVersion\\\":null,\\\"dispatchPeriod\\\":5,\\\"sampleFrequency\\\":100,\\\"logLevel\\\":\\\"WARNING\\\",\\\"anonymizeIP\\\":false,\\\"dryRun\\\":false}\"]}"
		},
      "namedIntTypes":{  
		
      }
   }
}