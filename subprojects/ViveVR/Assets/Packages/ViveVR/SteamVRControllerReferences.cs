﻿using System;
using UnityEngine;

public class SteamVRControllerReferences : MonoBehaviour
{
	//public HandControllerModel.HandControllerRelation whichHand;
	//public SteamVR_RenderModel RenderModel; //this is the thing I'm disabling
	public RefHandHolder leftHand;
	public RefHandHolder rightHand;

	[SerializeField]
	[Serializable]
	public class RefHandHolder
	{
		public GameObject handParent;
		public SteamVR_RenderModel renderModel;
	}
}