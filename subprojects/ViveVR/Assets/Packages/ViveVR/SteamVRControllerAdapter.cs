﻿using System;
using System.Collections;
using interfaces;
using UnityEngine;
using Valve.VR;
using Object = UnityEngine.Object;

namespace implementations
{
	[Implements(typeof(IControllerAdapter))]
	public class SteamVRControllerAdapter : IControllerAdapter
	{
		//for these actions, assume rightmost hand
		[Inject]
		public ActionInitiatedSignal ActionInitiated { get; set; }
		[Inject]
		public TimedActionInitiatedSignal TimedAction { get; set; }
		[Inject]
		public HandControllerModel ControllerModel { get; set; }

		[Inject]
		public IRoutineRunner runner { get; set; }

		[PostConstruct]
		public void PostConstruct()
		{
			runner.StartCoroutine(scanForPresses(SteamVR_Controller.DeviceRelation.FarthestLeft, ControllerModel.leftHand));
			runner.StartCoroutine(scanForPresses(SteamVR_Controller.DeviceRelation.FarthestRight, ControllerModel.rightHand));

			ControllerModel.leftHand.swapHandModel.AddListener(
				(GameObject go, Vector3 offset, Quaternion rotation) =>
				{
					swapHandModel(SteamVR_Controller.DeviceRelation.Leftmost, go, offset, rotation);
				}
				);
			ControllerModel.rightHand.swapHandModel.AddListener(
				(GameObject go, Vector3 offset, Quaternion rotation) =>
				{
					swapHandModel(SteamVR_Controller.DeviceRelation.Rightmost, go, offset, rotation);
				}
				);
			ControllerModel.leftHand.doVibrate.AddListener((delegate (HandControllerModel.HandModel.VibrateInfo info) { doVibrate(info, SteamVR_Controller.DeviceRelation.Leftmost); }));
			ControllerModel.rightHand.doVibrate.AddListener((delegate (HandControllerModel.HandModel.VibrateInfo info) { doVibrate(info, SteamVR_Controller.DeviceRelation.Rightmost); }));
		}

		void swapHandModel(SteamVR_Controller.DeviceRelation handRelation, GameObject go, Vector3 offset, Quaternion rotation)
		{
			//Debug.LogError("Handmodel swap attempt");
			var refs = Object.FindObjectOfType<SteamVRControllerReferences>();
			var refHand = handRelation == SteamVR_Controller.DeviceRelation.Rightmost ? refs.rightHand : refs.leftHand;
			refHand.renderModel.gameObject.SetActive(go != null);
			if(go != null)
			{
				runner.StartCoroutine(turnOffAsap(refHand.renderModel.gameObject));
				Debug.Log("parent:" + refHand.renderModel.transform.parent.gameObject.name);
				go.transform.parent = refHand.renderModel.transform.parent;
				go.transform.localRotation = rotation;
				go.transform.localPosition = offset;
			}
			else
			{
				Debug.Log("passed in argument is null");
			}
		}

		IEnumerator turnOffAsap(GameObject toSetOff)
		{
			for(int i = 0; i < 5; i++)
			{
				toSetOff.SetActive(false);
				yield return null;
			}
		}
		void doVibrate(HandControllerModel.HandModel.VibrateInfo info, SteamVR_Controller.DeviceRelation handDeviceRelation)
		{
			var controller = getControllerDevice(handDeviceRelation);
			if(controller == null)
				return;
			runner.StartCoroutine(doVibrateInternal(controller, info.durationInSeconds, info.numTimes));
		}
		private IEnumerator doVibrateInternal(SteamVR_Controller.Device device, float durationInSeconds = 0.5f, int numTicks = 3)
		{
			if(numTicks == 0)
			{
				Debug.LogWarning("num tikcs is zero, setting to one, since that's likely a mistake");
				numTicks = 1;
			}
			//0 and 1400 over muliple frames
			//bow is multiple ticks
			ushort microseconds = (ushort)(durationInSeconds * Math.Pow(10, 6));
			float remainingTime = durationInSeconds;
			for(int i = 0; i < numTicks; i++)
			{
				if(remainingTime <= 0f)
				{
					yield break;
				}
				device.TriggerHapticPulse(microseconds);
				yield return null;
				remainingTime -= Time.deltaTime;
			}
			if(remainingTime <= 0f)
			{
				yield break;
			}
			yield return new WaitForSeconds(remainingTime);
		}
		IEnumerator waitforTriggerUpAndPropigate(SteamVR_Controller.DeviceRelation relation, HandControllerModel.HandModel handModelToUpdate)
		{
			float timeDown = 0f;

			while(true)
			{
				var rightOrLeftController = SteamVR_Controller.GetDeviceIndex(relation);
				var controller = SteamVR_Controller.Input(rightOrLeftController);

				yield return null;
				if(controller.GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger))
				{
					Debug.LogError("Got mouse down before mouse up!");
					yield break;
				}

				if(controller.GetPressUp(EVRButtonId.k_EButton_SteamVR_Trigger))
				{
					handModelToUpdate.OnTriggerUp.Dispatch(timeDown);
					break;
				}
				timeDown += Time.deltaTime;
			}
			TimedAction.Dispatch(timeDown);
		}

		SteamVR_Controller.Device getControllerDevice(SteamVR_Controller.DeviceRelation relation)
		{
			var rightOrLeftController = SteamVR_Controller.GetDeviceIndex(relation);
			var controller = SteamVR_Controller.Input(rightOrLeftController);
			return controller;
		}
		//SteamVR_Controller.DeviceRelation.Rightmost
		private IEnumerator scanForPresses(SteamVR_Controller.DeviceRelation relation, HandControllerModel.HandModel handModelToUpdate)
		{
			while(true)
			{
				var controller = getControllerDevice(relation);
				if(controller != null)
				{

					if(controller.GetPressDown(EVRButtonId.k_EButton_SteamVR_Trigger))
					{
						Debug.Log("button pressed");
						ActionInitiated.Dispatch();
						handModelToUpdate.OnTriggerDown.Dispatch();
						runner.StartCoroutine(waitforTriggerUpAndPropigate(relation, handModelToUpdate)); //TODO: change so it aborts when contoller removed
					}
					handModelToUpdate.transfomInfo.position = controller.transform.pos;
					handModelToUpdate.transfomInfo.rotation = controller.transform.rot;
				}

				yield return null;
			}
		}
	}
}
