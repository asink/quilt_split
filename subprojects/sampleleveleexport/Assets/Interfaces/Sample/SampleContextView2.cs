﻿using interfaces;
using UnityEngine;

namespace implementations
{
	public class SampleContextView2 : HelloWorldContextView
	{
		[SerializeField] private InterfaceMappingUtilities mappingUtilities;

		protected new void Start()
		{
			mappingUtilities.SetFrameRate();
			mappingUtilities.InterfaceMap = mappingUtilities.GetDefaultMap();

			context = new SampleContext2(this, true, mappingUtilities);
			context.Start();
		}
	}
}