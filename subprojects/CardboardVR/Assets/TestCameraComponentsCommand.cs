﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using interfaces;
using strange.extensions.command.impl;
using UnityEngine;

namespace implementations
{
	public class TestCameraComponentsCommand : Command
	{
		[Inject]
		public IVR vr { get; set; }

		public override void Execute()
		{
			Debug.LogFormat(" right:{0}  left{1} rotation:{2} main camera name:{3} tag of maincamera:{4}", vr.leftCamera_LEGACY_HACK().name, vr.rightCamera_LEGACY_HACK().name, vr.Rotation(VRComponent.CENTER), Camera.main.gameObject.name, Camera.main.tag);
		}
	}
}
