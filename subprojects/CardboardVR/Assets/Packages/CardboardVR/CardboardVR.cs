﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters;
using interfaces;
using UnityEngine;

namespace implementations
{
	[Implements(typeof(IVR))]
	public class CardboardVR : IVR
	{
		private readonly CameraLoader _loader;
		private readonly List<CameraCommands> _supportedCommands = new List<CameraCommands>();
		private CameraSettingsSO _cameraSettings;
		private GameObject _cameraGO;

		private Dictionary<VRComponent, Transform> _vrTransforms = new Dictionary<VRComponent, Transform>
		{
			{VRComponent.LEFT_EYE, null},
			{VRComponent.RIGHT_EYE, null},
		};

		public CardboardVR()
		{
			_loader = new CameraLoader("CardboardVR");
		}

		public GameObject CreateCamera()
		{
			return _cameraGO ?? (_cameraGO = _loader.CreateCamera());
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
		}

		public void SetCustomCameraSettings(string settingString)
		{
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return _supportedCommands;
		}

		public Vector3 Position(VRComponent comp)
		{
			if(comp == VRComponent.CENTER)
			{
				var left = getComponent(VRComponent.LEFT_EYE).position;
				return left + (getComponent(VRComponent.RIGHT_EYE).position - left) / 2f;
			}
			return getComponent(comp).position;
		}

		public Quaternion Rotation(VRComponent comp)
		{
			return comp == VRComponent.CENTER ? getComponent(VRComponent.LEFT_EYE).rotation : getComponent(comp).rotation;
		}

		private delegate Transform findObjectCustom(string childName);
		private Transform getComponent(VRComponent component)
		{
			if(_vrTransforms[component] != null) return _vrTransforms[component];
			Transform newTransform = null;

			findObjectCustom gameObjectFinder = (string childName) =>
			{
				return findChildRecursive(_cameraGO.gameObject, childName); // GameObject.Find(childName).transform;
			};
			switch(component)
			{
				case VRComponent.LEFT_EYE:
					newTransform = gameObjectFinder("Camera Left");
					break;
				case VRComponent.RIGHT_EYE:
					newTransform = gameObjectFinder("Camera Right");
					break;
				//case VRComponent.CENTER:
				//	newTransform = gameObjectFinder("");
				default:
					throw new NotSupportedException("tried to get an unrecognized vr position:" + component);
			}
			_vrTransforms[component] = newTransform;
			return newTransform;
		}

		public Transform findChildRecursive(GameObject toTraverse, string name)
		{
			if (toTraverse.name == name)
			{
				return toTraverse.transform;
			}
			foreach (Transform child in toTraverse.transform)
			{
				if (child == toTraverse.transform) continue;
				var potenialchild = findChildRecursive(child.gameObject, name);
				if(potenialchild != null) return potenialchild;
			}
			return null;
		}
		public Transform leftCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.LEFT_EYE).transform;
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.RIGHT_EYE).transform;
		}
	}
}