﻿using UnityEngine;
using interfaces;
using UnityEditor;

//this is to help solve the issue of building on a headless server, where you can't manually set many of the things that you would normally do
public class SetAndroidSDKRoot : MonoBehaviour
{
	//cribbed from this
	//http://answers.unity3d.com/questions/495735/setting-android-sdk-path-in-environment-variable.html
	//example from my machine D:/dev/sdk/Android
	public static void SetRoot()
	{
		EditorPrefs.SetBool("OculusBuild", true);

		EditorApplication.SaveAssets();

		//if(!string.IsNullOrEmpty(EditorPrefs.GetString("AndroidSdkRoot")))
		//	return;
		EditorPrefs.SetString("AndroidSdkRoot", @"C:/Users/Administrator/AppData/Local/Android/android-sdk");


		EditorApplication.SaveAssets();

		Debug.Log("Set android sdk");
	}
	public static void AppSpecificBuild()
	{
		SetRoot();
		CIRunner.PerformConfigBuild();
	}
	public static void SetRootAndBuild()
	{
		SetRoot();
		CIRunner.PerformConfigBuild();
	}
}
