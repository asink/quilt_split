﻿using System;
using UnityEngine;
using UnityEditor;

namespace interfaces
{
	public class VersionHelperEditorScript
	{
		[MenuItem("quilt/extended/increment build numbers")]
		public static void incrementBuildNumber()
		{
			PlayerSettings.Android.bundleVersionCode++;
			PlayerSettings.iOS.buildNumber = (int.Parse(PlayerSettings.iOS.buildNumber) + 1).ToString();
			//PlayerSettings.bundleVersion = PlayerSettings.bundleVersion + 1;
			Version version = new Version(PlayerSettings.bundleVersion);
			PlayerSettings.bundleVersion = new Version(version.Major, version.Minor + 1).ToString();
			EditorApplication.SaveAssets();
		}
	}

}