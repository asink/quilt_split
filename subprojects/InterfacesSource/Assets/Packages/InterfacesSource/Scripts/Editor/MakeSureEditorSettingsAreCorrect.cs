﻿using UnityEditor;

[InitializeOnLoad]
public static class MakeSureEditorSettingsAreCorrect 
{
	static MakeSureEditorSettingsAreCorrect()
	{
		EditorSettings.serializationMode = SerializationMode.ForceText;
		EditorSettings.externalVersionControl = new ExternalVersionControl(ExternalVersionControl.Generic);
	}
}