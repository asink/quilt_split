﻿using System;

namespace interfaces
{
	public class CommandLineHelpers
	{
		public enum ParameterNames
		{
			BUILD_CONFIG_FILE_NAME,
			BUILD_NUMBER, //jenkins env var BUILD_NUMBER
			BUILD_TYPE,
			TECHNICAL_VERSION, //starts with git id, since I can pull that from jenkins  GIT_COMMIT 
			ANDROID_KEYSTORE,
			ANDROID_KEYSTORE_ALIAS,
			ANDROID_KEYSTORE_AND_ALIAS_PASSWORD,
			ANDROID_SDK_HOME, //ie C:/Program Files (x86)/Android/android-sdk unity: AndroidSdkRoot
			ANDROID_NDK_HOME, //ie C:/Program Files (x86)/Android/android-sdk unity: AndroidNdkRoot
			JDK_HOME, //java home ie C:\Program Files\Java\jdk1.8.0_25\       unity: JdkPath   EditorPrefs.GetString
		}

		private static readonly ILogger Log = new LogManager();
		
		public string GetStringFromCommandLine(ParameterNames name, bool lookInEnvironmetnAsWell = true)
		{
			return GetStringFromCommandLineUnsafe(name.ToString(), lookInEnvironmetnAsWell);
		}
		#region toReview
		private string getFlagStringFromNameUnsafe(string name)
		{
			return string.Format("-{0}=", name);
		}
		public string GetStringFromCommandLineUnsafe(string str,bool lookInEnvironmetnAsWell=true)
		{
			var flagStringParamName = getFlagStringFromNameUnsafe(str);
			
			try
			{
				var args = Environment.GetCommandLineArgs();
				foreach(var arg in args)
				{
					if(!arg.Contains(flagStringParamName)) continue;
					var targetString = arg.Replace(flagStringParamName, "");

					return targetString;
				}
				if(lookInEnvironmetnAsWell)
				{
					//favor command line before environment, normal ordering 
					var env = Environment.GetEnvironmentVariable(str);
					if(!string.IsNullOrEmpty(env))
						return env;
				}
			}
			catch(Exception e)
			{
				Log.LogError("Error getting " + str + " ( " + flagStringParamName + ") : Exception-" + e);
				throw;
			}
			throw new Exception("Could not find " + str + " ( " + flagStringParamName + ") in command line arguments");
		}
#endregion //toreview

		public int GetIntFromCommandLine(ParameterNames name)
		{
			try
			{
				return int.Parse(GetStringFromCommandLine(name));
			}
			catch(Exception e)
			{
				Log.LogError("Error getting " + name + " : Exception-" + e);
				throw;
			}
		}
	}
}