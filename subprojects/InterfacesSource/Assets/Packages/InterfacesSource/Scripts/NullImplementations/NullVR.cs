﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VR;

namespace interfaces
{
	public class CameraLoader
	{
		private readonly string _prefabName;
		private CameraSettingsSO _cameraSettings;

		public CameraLoader(string prefabName)
		{
			_prefabName = prefabName;
		}
		
		public GameObject CameraRef { get; private set; }

		public CameraSettingsSO CameraSettings
		{
			get
			{
				if (!_cameraSettings || _cameraSettings == null) _cameraSettings = Resources.Load<CameraSettingsSO>(_prefabName);
				return _cameraSettings;
			}
		}

		public GameObject CreateCamera()
		{
			if (CameraRef != null)
				return CameraRef;
			CameraRef = CameraSettings.Create();
			return CameraRef;
		}
	}

	//[Implements(typeof(IVR), InjectionBindingScope.CROSS_CONTEXT)]
	public class NullVR : IVR
	{
		private readonly CameraLoader _loader;
		private readonly List<CameraCommands> _supportedCommands = new List<CameraCommands>(){CameraCommands.RESET_TRACKING};
		private Transform _cameraTransform;

		public NullVR()
		{
			_loader = new CameraLoader("NullVR");
		}

		public GameObject CreateCamera()
		{
			return _loader.CreateCamera();
		}

		public CameraSettingsSO GetCameraSettingsSO()
		{
			return _loader.CameraSettings;
		}

		public void CameraCommand(CameraCommands command)
		{
			switch (command)
			{
					case CameraCommands.RESET_TRACKING:
						InputTracking.Recenter();
					break;
				default:
					break;
			}
		}

		public void SetCustomCameraSettings(string settingString)
		{
		}

		public List<CameraCommands> GetSupportedCommands()
		{
			return _supportedCommands;
		}

		public Vector3 Position(VRComponent pos)
		{
			return getComponent(pos).position;
		}

		public Quaternion Rotation(VRComponent pos)
		{
			return getComponent(pos).localRotation;
		}

		public Transform leftCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.LEFT_EYE).transform;
		}

		public Transform rightCamera_LEGACY_HACK()
		{
			return getComponent(VRComponent.RIGHT_EYE).transform;
		}

		public Transform CameraTransform()
		{
			if (_loader.CameraRef == null)
			{
				return null;
			}
			
			return _cameraTransform ?? (_cameraTransform = _loader.CameraRef.transform);
		}

		private Transform getComponent(VRComponent component)
		{
			switch (component)
			{
				case VRComponent.LEFT_EYE:
				case VRComponent.RIGHT_EYE:
				case VRComponent.CENTER:
					return CameraTransform();
				default:
					throw new NotSupportedException("tried to get an unrecognized vr position:" + component);
			}
		}
		public Vector3 LocalPosition(VRComponent pos)
		{
			return getComponent(pos).localPosition;
		}
	}
}