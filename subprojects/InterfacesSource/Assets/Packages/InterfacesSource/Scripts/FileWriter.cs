﻿using System;
using System.IO;
using System.Text;
using UnityEngine;

namespace interfaces
{
	public class FileWriter : IDisposable
	{
		private readonly string _filePrefix;
		private readonly int _maxLogEntries;
		private string _fileName;
		private FileStream _fs;
		private int _logEntriesWritten;
		private StreamWriter _writer;

		public FileWriter(string filePrefix)
		{
			_filePrefix = filePrefix;
		}

		public FileWriter(string filePrefix, int maxLogEntries)
		{
			_filePrefix = filePrefix;
			_maxLogEntries = maxLogEntries;
		}

		public string FileName
		{
			get { return _filePrefix; }
		}

		public void Dispose()
		{
			try
			{
				if (_writer != null)
				{
					_writer.Flush();
					_writer.Close();
				}
				//if(_fs != null)
				//{
				//	_fs.Flush();
				//	_fs.Close();
				//}
				if (_writer != null)
				{
					_writer.Dispose();
					_writer = null;
				}
				if (_fs != null)
				{
					_fs.Dispose();
					_fs = null;
				}
			}
			catch (Exception e)
			{
				Debug.Log("error disposing in moviesessionlogger:" + e);
			}
		}

		public void Init()
		{
			if (_fs == null)
			{
				_fileName = string.Format(_filePrefix + "_{0}_{1}_{2}.txt", SystemInfo.deviceUniqueIdentifier, Application.platform,
					DateTime.Now.ToString("yyyy_M_d_HH_mm_ss"));
				_fs = new FileStream(Application.temporaryCachePath + Path.DirectorySeparatorChar + _fileName, FileMode.CreateNew);
			}
			if (_writer == null)
				_writer = new StreamWriter(_fs, Encoding.UTF8);
			_writer.AutoFlush = true;
		}

		public void Flush()
		{
			if (_writer != null)
			{
				_writer.Flush();
			}	
		}

		public void Write(string toWrite)
		{
			if (_maxLogEntries > 0 && _logEntriesWritten >= _maxLogEntries)
			{
				Dispose();
				_logEntriesWritten = 0;
			}
			
			Init();

			_writer.WriteLine(toWrite);
			_logEntriesWritten++;
			//if(_logEntriesWritten % 10 == 0)
			//{
			_writer.Flush();
			_fs.Flush(); //flush file explicitly
			//}
		}
	}
}