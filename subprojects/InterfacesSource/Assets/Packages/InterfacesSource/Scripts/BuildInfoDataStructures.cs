﻿using System;
using System.Collections.Generic;
using UnityEditorCopies;

namespace interfaces
{
	[Serializable]
	public class BuildConfiguration
	{
		//for builder
		//maybe add the scenes to use explicitly?
		public string app_output_name; //ie MyApp or MyApp.apk depending on the platform
		public SerializedPlayerSettings buildConfig;
		public BuildOptions_Copy buildOptions;
		//http://docs.unity3d.com/ScriptReference/BuildOptions.html uncompressed, allow debugging, etc

		public SerializedEditorUserBuildSettings editorUserBuildSettings;
		public InterfaceMap InterfaceMapping;
		//for first pass installation of packages
		public List<string> packagesToAdd;
		public string qualitySettingsDefault;
	}

	//so we can run external builds against multiple skus from a config file
	//from http://docs.unity3d.com/ScriptReference/EditorUserBuildSettings.html
	[Serializable]
	public class SerializedEditorUserBuildSettings
	{
		public bool allowDebugging;
		public BuildTarget_Copy currentBuildTarget;
		public BuildTargetGroup_Copy currentBuildTargetGroup;
		//assuming EditorUserBuildSettings.selectedBuildTargetGroup gets set when we switch groups

		//public bool appendProject;  //is now BiuldOptions.AcceptExternalModificationsToPlayer
		public bool development;
		public bool explicitNullChecks;
		public MobileTextureSubtarget_Copy subtarget; //was: activeBuildTarget
		public bool symlinkLibraries;
		public bool vrEnabled;
		public VRDeviceType_Copy vrDeviceType;
	}

	//TODO: Handheld -- this is where use32BitDisplayBuffer	,GetActivityIndicatorStyle, etc all work
	//getting,setting, saving PlayerSettings to allow for different SKUs http://docs.unity3d.com/ScriptReference/PlayerSettings.html
	//NOTE: this should probably be set after setting up the EidtorUserBuildSettings
	//TODO: things with no setters ie gpuskinning
	[Serializable]
	public class SerializedPlayerSettings
	{
		public string bundleIdentifier = "com.asink.unspecifiedBuild";
		//public string bundleVersion = "1.0";
		public string companyName = "quilt";
		public string defaultIconGUID;
		public UIOrientation_Copy defaultInterfaceOrientation = UIOrientation_Copy.AutoRotation;
		public bool defaultIsFullScreen;
		public int defaultScreenHeight;
		public int defaultScreenWidth;
		public ResolutionDialogSetting_Copy displayResolutionDialog; //controls splash screen
		public GraphicsDeviceType_Copy[] graphicsAPIs;
		//public string serializedGUID;
		//public UIOrientation defaultInterfaceOrientation = UIOrientation.LandscapeLeft;
		public string[] iconGroupGUIDs;
		public iOSTargetDevice_Copy iosTargetDevice; //iphone,ipad, or iphoneandipad
		public iPhoneArchitecture_Copy iPhoneArchitecture; //ARMv7,ARM64, or Universal
		public string productName;
		public RenderingPath_Copy RenderingPathCopy;
		public RenderingPath_Copy MobileRenderingPathCopy;
		//public ScriptCallOptimizationLevel_Copy scriptCallOptimizationLevel; //slowandsfe or fastbutnoexceptions, removed since it's not implemented
		public ScriptingImplementation_Copy scriptingBackend;
		public List<string> scriptingDefineSymbols;
		public string splashScreenGUIDAkaResolutionDialogBanner; //ie resolutionDialogBanner
		public bool stripUnusedMeshComponents;
		public List<AspectRatio_Copy> supportedAspectRatios;
		public bool use32BitDisplayBuffer;
		public bool usePlayerLog;
		public bool virtualRealitySupported;
		//5.2 setting that should almost never be used for vr.  ensuring that it gets turned on, since upgraded projects will get this set to "on" during the update
		public bool resizableWindow;
	}
}