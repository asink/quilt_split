﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace interfaces
{
	public interface IVR
	{
		GameObject CreateCamera();
		CameraSettingsSO GetCameraSettingsSO();
		//known and common camera commands we may want to set
		void CameraCommand(CameraCommands command);
		//"ioctl" equivilant -- random functional side-effects
		void SetCustomCameraSettings(string settingString);
		//for commands that aren't supported officially by this api yet, or are unusual

		//cameras will have different features
		List<CameraCommands> GetSupportedCommands();
		//hacky thing for now to allow me to look for all camerasettings in the project and exclude one, or alternatively just grab this, since it should be on the nose
		Vector3 Position(VRComponent component);
		Quaternion Rotation(VRComponent component);

		//unitil untiy5.1 allows masks on individual cameras
		Transform leftCamera_LEGACY_HACK();
		Transform rightCamera_LEGACY_HACK();
	}

	public enum CameraCommands
	{
		//more standard
		DISMISS_HSW = 100,
		RESET_TRACKING,
		//general performance
		THROTTLE_DOWN = 200,
		THROTTLE_DOWN_CPU = 201,
		THROTTLE_DOWN_GPU = 202,
		STANDARD_PERFORMANCE = 203,

		//rendering optimizations
		EYE_AA_ONE = 300,
		EYE_AA_TWO = 301,
		EYE_TEXTURE_DEPTH_16 = 302,
		EYE_TEXTURE_DEPTH_24 = 303
	}

	public enum VRComponent
	{
		LEFT_EYE = 100,
		RIGHT_EYE = 200,
		CENTER = 300
	}

	//allow me to set per-application or per-mode settings on the cameras without direct access
	//http://docs.unity3d.com/Manual/class-Camera.html for all possible fields I may want too use in the future
	[Serializable]
	public class SerializedCameraSettings
	{
		public float[] backgroundColor;
		public CameraClearFlags clearFlags;
		public string customCameraSettings; //ie flipy for oculus vr camera
		public float depth;
		//public float fieldOfView;
		public float farClipPlane;
		public string textureResourcesName;

		public Color getCameraBackgroundColor()
		{
			return backgroundColor == null
				? Color.white
				: new Color(backgroundColor[0], backgroundColor[1], backgroundColor[2], backgroundColor[3]);
		}

		public void SaveCameraBackgroundColor(Color color)
		{
			backgroundColor = new[] {color.r, color.g, color.b, color.a};
		}
	}

	public class CameraUtility
	{
		public static void SetCameraSettingsOnAllCameras(SerializedCameraSettings settings, GameObject parent)
		{
			var cameras = parent.GetComponentsInChildren<Camera>();
			foreach (var vrcamera in cameras)
			{
				//HACK to get the cardboard camera to work as expected -- latest one has post/pre render
				//This should be transitioned over to the component system - grab explicitly tagged cameras if possible
				if (vrcamera.transform.parent != null && vrcamera.transform.parent.gameObject.name.Contains("Stereo Render"))
				{
					continue;
				}
				if(settings.backgroundColor != null)
					vrcamera.backgroundColor = settings.getCameraBackgroundColor();
				vrcamera.clearFlags = settings.clearFlags;

				if (settings.farClipPlane > 0)
				{
					vrcamera.farClipPlane = settings.farClipPlane;
					//NOTE: almost every camera type seems to hard error whith this set to zero, so I don't dtho that for sure in code.  was the last but at ggj
				}

				if (settings.clearFlags == CameraClearFlags.Skybox && !string.IsNullOrEmpty(settings.textureResourcesName))
				{
					var skyboxMat = Resources.Load<Material>(settings.textureResourcesName);
					var newSB = vrcamera.gameObject.AddComponent<Skybox>();
					newSB.material = skyboxMat;
				}

				//camera.fieldOfView = settings.fieldOfView;
			}
		}
	}
}