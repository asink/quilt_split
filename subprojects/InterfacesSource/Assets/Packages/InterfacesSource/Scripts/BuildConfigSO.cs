﻿using System.Collections.Generic;
using UnityEngine;

namespace interfaces
{
	//TODO: property drawer and SerializedProperty example
	public class BuildConfigSO : ScriptableObject
	{
#pragma warning disable 0649

		public enum BuildTypes
		{
			Development,
			Testing,
			ReleaseCandidate,
			Release,
		}

		//[EnumPopup(BuildTypes)]
		public BuildTypes BuildType;

		//[Popup("String", "String1", "String2", "String3", "String4")]
		public int BuildNumber;
		public string TechnicalVersion; //git commit id from cli

		public string ProductName;

		public BuildConfiguration buildConfig;
		public string packageFileName;

		public Dictionary<string, string> IdentifierStrings()
		{
			return new Dictionary<string, string>
			{
				{"ProductName", ProductName},
				{"User", SystemInfo.deviceUniqueIdentifier},
				{"Platform", Application.platform.ToString()},
				{"BuildNumber", BuildNumber.ToString()},
				{"BuildFile",packageFileName },
				{"BuildType", BuildType.ToString()},
				{"TechnicalVersion", TechnicalVersion },
			};
		} 
#pragma warning restore 0649
	}

}