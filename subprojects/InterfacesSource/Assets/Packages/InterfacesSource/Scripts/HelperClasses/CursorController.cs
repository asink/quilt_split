﻿using System.Collections;
using UnityEngine;

namespace interfaces
{
	public class CursorController : MonoBehaviour
	{
		[SerializeField]
		private GameObject _cursor;

		private MoveUnderneathCamera _cursorMover;
		private SpriteRenderer _myRenderer;
		public Sprite RegularSprite;
		public Sprite SpinSprite;

		public void Awake()
		{
			_cursorMover = new MoveUnderneathCamera();
			_myRenderer = _cursor.GetComponent<SpriteRenderer>();

			_myRenderer.enabled = false;
		}

		public void SetCursorSprite(Sprite RegularSpriteIn,Sprite SpinSpriteIn)
		{
			if (_myRenderer.sprite == RegularSprite)
				_myRenderer.sprite = RegularSpriteIn;
			if (_myRenderer.sprite == SpinSpriteIn)
				_myRenderer.sprite = SpinSpriteIn;

			RegularSprite = RegularSpriteIn;
			SpinSprite = SpinSpriteIn;
		}
		[ContextMenu("Show cursor")]
		void contextMenuShowCursor()
		{
			ShowCursor(Camera.main.transform);
		}
		public void ShowCursor(Transform newParent)
		{
			Debug.Log("ShowCursor");
			_cursorMover.MoveInCenter(_cursor, Vector3.forward, newParent);
			_myRenderer.enabled = true;
		}

		[ContextMenu("Hide cursor")]
		public void HideCursor()
		{
			_cursorMover.ResetToOriginal(_cursor);
			_myRenderer.enabled = false;
		}

		[ContextMenu("Spin Cursor")]
		public void Spin()
		{
			SpinCursor(10f,Camera.main.transform);
		}

		public void SpinCursor(float sequenceRunTime, Transform newParent)
		{
			StartCoroutine(DoSpin(sequenceRunTime, newParent));
		}

		private IEnumerator DoSpin(float time, Transform newParent)
		{
			_myRenderer.sprite = SpinSprite;
			ShowCursor(newParent);

			var originalScale = _cursor.transform.localScale;
			var originalRotation = _cursor.transform.localRotation;
			const float cycleTime = 1f;

			var timeElapsed = 0f;
			while(timeElapsed < time)
			{
				var normalizedTime = (timeElapsed % cycleTime) / cycleTime;
				if(normalizedTime > 0.5f)
					_cursor.transform.localScale = Mathf.Lerp(1f, 2f, normalizedTime * 2f) * Vector3.one;
				else
				{
					var normalizedTimeOverHalf = normalizedTime - (normalizedTime / 2f);
					_cursor.transform.localScale = Mathf.Lerp(2f, 1f, normalizedTimeOverHalf * 2f) * Vector3.one;
				}
				_cursor.transform.Rotate(Vector3.forward * Time.deltaTime * 40f);
				yield return null;
				timeElapsed += Time.deltaTime;
			}
			_cursor.transform.localScale = originalScale;
			_cursor.transform.localRotation = originalRotation;

			_myRenderer.sprite = RegularSprite;
			HideCursor();
		}
	}
}