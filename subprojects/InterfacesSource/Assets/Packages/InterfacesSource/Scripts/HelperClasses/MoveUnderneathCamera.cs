﻿using UnityEngine;

namespace interfaces
{
	public class MoveUnderneathCamera
	{
		private Transform _originalParent;

		public void MoveInCenter(GameObject toMove, Vector3 localOffsetFromCamera, Transform cameraRotationParent)
		{
			this._originalParent = toMove.transform.parent;

			toMove.transform.parent = cameraRotationParent;
			toMove.transform.localEulerAngles = Vector3.zero;
			toMove.transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);
			toMove.transform.localPosition = localOffsetFromCamera;
		}

		public void ResetToOriginal(GameObject toMove)
		{
			toMove.transform.parent = this._originalParent;
		}
	}
}