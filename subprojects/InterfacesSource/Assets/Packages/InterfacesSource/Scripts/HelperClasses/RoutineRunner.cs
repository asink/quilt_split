﻿using System.Collections;
using UnityEngine;

namespace interfaces
{
	public interface IRoutineRunner
	{
		Coroutine StartCoroutine(IEnumerator toRun);
		void StopCoroutine(IEnumerator toStop);
	}

	//[Implements(typeof(IRoutineRunner))]
	public class RoutineRunner : MonoBehaviour, IRoutineRunner
	{
	}
}