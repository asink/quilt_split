﻿using System;
using System.Collections.Generic;
using System.Reflection;
using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.impl;
using strange.extensions.signal.impl;
using UnityEngine;

namespace interfaces
{
	//Base Class for all our Contexts
	//We do two jobs here:
	//1. Provide the important bindings for a Signals-based app (see http://strangeioc.wordpress.com/2013/09/18/signals-vs-events-a-strange-smackdown-part-2-of-2/)
	//2. Scan for Implicit Bindings (see http://strangeioc.wordpress.com/2013/12/16/implicitly-delicious/)

	public class SignalContext : MVCSContext
	{
		public List<string> AssembliesWhitelistContains = new List<string> {"jsonfx", "strange", "interfaces", "Assembly-"};
		public List<string> NamespacesToScan = new List<string> {"interfaces", "implementations"};
		public List<string> AssemblyBlacklist = new List<string> {"Editor"};

		public SignalContext()
		{
		}

		public SignalContext(MonoBehaviour contextView) : base(contextView)
		{
		}

		public SignalContext(MonoBehaviour contextView, bool autostart) : base(contextView, autostart)
		{
		}

		protected override void addCoreComponents()
		{
			base.addCoreComponents();
			injectionBinder.Unbind<ICommandBinder>();
			injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
		}

		public override void Launch()
		{
			base.Launch();
			var startSignal = injectionBinder.GetInstance<StartSignal>();
			startSignal.Dispatch();
		}

		protected override void mapBindings()
		{
			base.mapBindings();
			var assemblies = getAssembliesUsingBlackWhiteLists();
			for (var j = 0; j < assemblies.Count; j++)
			{
				var assembly = assemblies[j];
				implicitBinder.Assembly = assembly;
				implicitBinder.ScanForAnnotatedClasses(NamespacesToScan.ToArray());
			}
			implicitBinder.Assembly = Assembly.GetExecutingAssembly();
		}

		private List<Assembly> getAssembliesUsingBlackWhiteLists()
		{
			var assemblies = new List<Assembly> {Assembly.GetExecutingAssembly()};
			foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies())
			{
				var name = assembly.GetName().Name;
				if(assemblies.Contains(assembly))
				{
					continue;
				}
				foreach (var assembliesWhitelistContain in AssembliesWhitelistContains)
				{
					if(name.ToLower().Contains(assembliesWhitelistContain.ToLower()))
					{
						assemblies.Add(assembly);
						break;
					}
				}
			}
			var toBlacklist = new List<Assembly>();
			foreach (var assembly in assemblies)
			{
				foreach (string blacklisted in AssemblyBlacklist)
				{
					if(assembly.GetName().Name.ToLower().Contains(blacklisted.ToLower()))
					{
						toBlacklist.Add(assembly);
					}
				}
			}
			foreach (var blacklist in toBlacklist)
			{
				assemblies.Remove(blacklist);
			}
			//foreach (var assembly in assemblies)
			//{
			//	Debug.LogError(assembly.FullName);
			//}
			return assemblies;
		}
	}

	public class StartSignal : Signal
	{
	}
}