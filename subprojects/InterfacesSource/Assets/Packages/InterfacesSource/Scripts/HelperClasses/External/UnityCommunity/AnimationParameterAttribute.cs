﻿using UnityEngine;

namespace interfaces
{
	public class AnimationParameterAttribute : PropertyAttribute
	{
		public string animatorProp;

		public AnimationParameterAttribute(string animatorPropName)
		{
			animatorProp = animatorPropName;
		}
	}
}