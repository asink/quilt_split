﻿using System.Collections;
using strange.extensions.command.impl;
using System.Collections.Generic;
using UnityEngine;

namespace interfaces
{
	public class LogClientInfoCommand : Command
	{
		[Inject]
		public IAnalytics analytics { get; set; }

		[Inject]
		public ILogger logger { get; set; }
		[Inject]
		public BuildConfigSO buildConfig { get; set; }
		[Inject]
		public IMonobehaviourLifecycleMessages lifecycle { get; set; }
		[Inject(NamedInjectionsCore.PLATFORM)]
		public string platform { get; set; }

		public override void Execute()
		{
			var clientInfo = buildConfig.IdentifierStrings();
			analytics.StartSession();
			analytics.Log("clientinfo", clientInfo);
			logger.Log(clientInfo.JsonSerialize());
		}
	}
}