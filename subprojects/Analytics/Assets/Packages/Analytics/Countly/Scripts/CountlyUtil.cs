﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CountlyUtil
{

    /// <summary>
    /// Executes a coroutine.
    /// </summary>
    /// <param name="coroutine">Coroutine to execute</param>
    public static Coroutine RunCoroutine(IEnumerator coroutine)
    {
        if (Countly.Instance == null)
        {
            Countly.Log("Countly game object must be instantiated " +
                           "before its methods can be called.");
            return null;
        }

        return Countly.Instance.StartCoroutine(coroutine);
    }

    /// <summary>
    /// Stores device information
    /// </summary>
    public static class DeviceInformation
    {
        public static string UDID;
        public static string OS;
        public static string OSVersion;
        public static string Device;
        public static string Resolution;
        public static string Carrier;
        public static string Locale;
    }

    /// <summary>
    /// Detects and sets DeviceInformation
    /// </summary>
    public static void DetectDevice()
    {
        DeviceInformation.UDID = SystemInfo.deviceUniqueIdentifier;
        DeviceInformation.OSVersion = Environment.OSVersion.Version.ToString();
        DeviceInformation.Resolution = Screen.currentResolution.width + "x" + Screen.currentResolution.height;
        DeviceInformation.Carrier = null;
        DeviceInformation.Locale = Application.systemLanguage.ToString();

#if UNITY_EDITOR
        DeviceInformation.Device = "Unity Editor";
        DeviceInformation.OS = "Unity Editor";
#endif

#if UNITY_IPHONE
        DeviceInformation.Device = iPhone.generation.ToString();
        DeviceInformation.OS = "iOS";
        DeviceInformation.OSVersion = SystemInfo.operatingSystem;
#endif

#if UNITY_STANDALONE_OSX
	    DeviceInformation.Device = "MAC";
        DeviceInformation.OS = "Mac OS";
#endif

#if UNITY_STANDALONE_WIN
        DeviceInformation.Device = "PC";
        DeviceInformation.OS = "Windows";
#endif
    }

    public static string GetMetrics(string appVersion)
    {
        string jSONSeriliazation = "{";

        jSONSeriliazation += "\"" + "_device" + "\"" + ":" + "\"" + DeviceInformation.Device + "\"";
        jSONSeriliazation += "," + "\"" + "_os" + "\"" + ":" + "\"" + DeviceInformation.OS + "\"";
        jSONSeriliazation += "," + "\"" + "_os_version" + "\"" + ":" + "\"" + DeviceInformation.OSVersion + "\"";
        jSONSeriliazation += "," + "\"" + "_carrier" + "\"" + ":" + "\"" + DeviceInformation.Carrier + "\"";
        jSONSeriliazation += "," + "\"" + "_resolution" + "\"" + ":" + "\"" + DeviceInformation.Resolution + "\"";
        jSONSeriliazation += "," + "\"" + "_local" + "\"" + ":" + "\"" + DeviceInformation.Locale + "\"";
        jSONSeriliazation += "," + "\"" + "_app_version" + "\"" + ":" + "\"" + appVersion + "\"";
        jSONSeriliazation += "}";

        return jSONSeriliazation;
    }

    public static string SeriliazeEvents(List<CountlyEvent> events)
    {
        string jSONSeriliazation = String.Empty;

        bool first1 = true;

        jSONSeriliazation += "[";

        foreach (CountlyEvent CurrentEvent in events)
        {
            if (first1)
            {
                first1 = false;
            }
            else
            {
                jSONSeriliazation += ",";
            }

            jSONSeriliazation += "{";
            jSONSeriliazation += "\"" + "key" + "\"" + ":" + "\"" + CurrentEvent.Key + "\"" + ",";
            jSONSeriliazation += "\"" + "count" + "\"" + ":" + CurrentEvent.Count;
            if (CurrentEvent.UsingSum)
            {
                jSONSeriliazation += ",";
                jSONSeriliazation += "\"" + "sum" + "\"" + ":" + CurrentEvent.Sum;
            }
            if (CurrentEvent.UsingSegmentation)
            {
                jSONSeriliazation += ",";
                jSONSeriliazation += "\"" + "segmentation" + "\"" + ":" + "{";

                bool first2 = true;
                foreach (String currentKey in CurrentEvent.Segmentation.Keys)
                {
                    if (first2)
                    {
                        first2 = false;
                    }
                    else
                    {
                        jSONSeriliazation += ",";
                    }
                    jSONSeriliazation += "\"" + currentKey + "\"" + ":" + "\"" + CurrentEvent.Segmentation[currentKey] + "\"";
                }

                jSONSeriliazation += "}";
            }
            jSONSeriliazation += "}";
        }

        jSONSeriliazation += "]";

        return jSONSeriliazation;
    }

}

