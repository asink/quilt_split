using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using System.Collections;

public class Countly : MonoBehaviour
{
    public static Countly Instance { get; private set; }

    /// <summary>
    /// Server URL: Set your server URL. 
    /// Enter https://cloud.count.ly for Countly Cloud.
    /// </summary>
    public string ServerURL;

    /// <summary>
    /// Your Application Key. 
    /// You can get it from Countly web site after login. "Management -> Application -> Your Application -> Application Key"
    /// </summary>
    public string AppKey;

    /// <summary>
    /// Your application version.
    /// </summary>
    public string AppVersion;

    /// <summary>
    /// Countly waits some time after failed server connection. 
    /// For each failed try it increases waiting time by multiplying this value with number of failed tries. 
    /// You can change this duration by changing this value. (Default: 5)
    /// </summary>
    public float SleepAfterFailedTry = 5f;

    /// <summary>
    /// Countly sends kind of Keep Alive messages to server to calculate session lengths. 
    /// You can change the period by changing this value. (Default: 10)
    /// </summary>
    public float DataCheckPeriod = 10f;

    /// <summary>
    /// Countly checks that is there any data waiting to send server and sends them with this period. 
    /// </summary>
    public float KeepAliveSendPeriod = 30f;

    /// <summary>
    /// Allows you to see what is going on. 
    /// </summary>
    public bool IsDebugModeOn = false;

    /// <summary>
    /// Calls OnStart() and OnStop() methods automatically.
    /// </summary>
    public bool AutoStart = true;

    public Queue<string> ConnectionQueue;
    public bool IsCountlyWorking;
    public int PackageSizeForEvents = 5;

    private List<CountlyEvent> _eventQueue;

    private float _lastSessionLengthSentOn;

    /// <summary>
    /// Can countly send data to server
    /// </summary>
    public static bool SendDataToServer;
    
    /// <summary>
	/// Initializes a new instance of this class.
	/// </summary>
	Countly () {}

    /// <summary>
    /// Sets up Countly.
    /// </summary>
    void Awake()
    {
        // Prevent multiple instances of Countly from existing.
        // Necessary because DontDestroyOnLoad keeps the object between scenes.
        if (Instance != null)
        {
            Debug.Log("Destroying duplicate game object instance.");
            Destroy(gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);

        CountlyUtil.DetectDevice();

        ConnectionQueue = new Queue<string>();
        _eventQueue = new List<CountlyEvent>();

    }

    void Start()
    {
        if (AutoStart && ServerURL != null && AppKey != null && AppVersion != null)
        {
            //AUTO START
            OnStart();
        }
        
    }

    void OnDestroy()
    {
        Log("Destroying...");
        Instance.OnStop();
    }

    /// <summary>
    /// Initializes countly. You can set these values from inspector as well
    /// </summary>
    /// <param name="serverURL">Your Server URL</param>
    /// <param name="appKey">Your Application Key</param>
    /// <param name="appVersion">Your Application Version</param>
    public void Init(string serverURL, string appKey, string appVersion)
    {
        Instance.ServerURL = serverURL;
        Instance.AppKey = appKey;
        Instance.AppVersion = appVersion;
    }

    /// <summary>
    /// Starts Countly (Manual Mode)
    /// </summary>
    public void OnStart()
    {
        Log("Starting...");

        BeginSession();

        IsCountlyWorking = true; 
        
        CountlyUtil.RunCoroutine(CountlyWWW.SendDataToServer());
        CountlyUtil.RunCoroutine(KeepAlive());

        SendDataToServer = true;
    }

    /// <summary>
    /// Stops Countly (Manual Mode)
    /// </summary>
    public void OnStop()
    {
        Log("Stopping...");

        IsCountlyWorking = false;
        
        EndSession();

        SendDataToServer = false;
    }

    /// <summary>
    /// Posts events
    /// </summary>
    /// <param name="Event">Event to post</param>
    public void PostEvent(CountlyEvent Event)
    {
        if (Event == null)
        {
            throw new ArgumentNullException("Event");
        }

        Log("Event Posted -> " + Event.Key);

        _eventQueue.Add(Event);

        if (_eventQueue.Count >= PackageSizeForEvents)
        {
            QueueEvents();
            _eventQueue.Clear();
        }
    }

    public static IEnumerator KeepAlive()
    {
        //Log("Keeping Alive...");
        
        UpdateSession();

        yield return new WaitForSeconds(Instance.KeepAliveSendPeriod);

        if (Instance.IsCountlyWorking)
        {
            CountlyUtil.RunCoroutine(KeepAlive());
        }
    }
    
    private static void BeginSession()
    {
        string data = String.Format("app_key={0}&device_id={1}&sdk_version=1.0&begin_session=1&metrics={2}", Instance.AppKey, CountlyUtil.DeviceInformation.UDID, WWW.EscapeURL(CountlyUtil.GetMetrics(Instance.AppVersion)));
        Instance.ConnectionQueue.Enqueue(data);

        Instance._lastSessionLengthSentOn = Time.time;
    }

    private static void UpdateSession()
    {
        int duration = Mathf.RoundToInt(Time.time - Instance._lastSessionLengthSentOn);
        
        string data = String.Format("app_key={0}&device_id={1}&session_duration={2}", Instance.AppKey, CountlyUtil.DeviceInformation.UDID, duration);
        Instance.ConnectionQueue.Enqueue(data);

        Instance._lastSessionLengthSentOn = Time.time;
    }

    private static void EndSession()
    {
        int duration = Mathf.RoundToInt(Time.time - Instance._lastSessionLengthSentOn);
        string data = String.Format("app_key={0}&device_id={1}&end_session=1&session_duration={2}", Instance.AppKey, CountlyUtil.DeviceInformation.UDID, duration);

        if (Instance._eventQueue.Count > 0)
        {
            string events = CountlyUtil.SeriliazeEvents(Instance._eventQueue);
            Instance._eventQueue.Clear();

            data += "&events=" + WWW.EscapeURL(events);
        }

        Instance.ConnectionQueue.Enqueue(data);

        CountlyWWW.Send(data, 250);

    }

    private void QueueEvents()
    {
        string data = String.Format("app_key={0}&device_id={1}&events={2}", AppKey, CountlyUtil.DeviceInformation.UDID, WWW.EscapeURL(CountlyUtil.SeriliazeEvents(_eventQueue)));
        
        Log("Events are packaged. Package is sending...");

        Instance.ConnectionQueue.Enqueue(data);
    }

    /// <summary>
    /// Logs messages. 
    /// Works only in debug mode.
    /// </summary>
    /// <param name="message"></param>
    public static void Log(string message)
    {
        if (Instance.IsDebugModeOn)
        {
            Debug.Log("Countly:\t" + message);
        }
    }
}
